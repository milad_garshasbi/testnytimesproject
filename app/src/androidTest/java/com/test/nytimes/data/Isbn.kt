package com.test.nytimes.data

data class Isbn(
    val isbn10: String = "",
    val isbn13: String = ""
)