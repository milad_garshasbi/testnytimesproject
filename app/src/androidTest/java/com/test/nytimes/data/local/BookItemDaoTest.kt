package com.test.nytimes.data.local

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import com.test.nytimes.data.MockBookItem
import com.test.nytimes.getOrAwaitValue
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject
import javax.inject.Named

@ExperimentalCoroutinesApi
@SmallTest
@HiltAndroidTest
class BookItemDaoTest {


    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Inject
    @Named("test_db")
    lateinit var database: BookItemsDataBase

    private lateinit var dao: BookItemDao

    @Before
    fun setup() {
        hiltRule.inject()
        dao = database.booksDao()
    }

    @After
    fun tearDown() {
        database.close()
    }

    @Test
    fun insertBookItem() = runBlockingTest {
        val book = MockBookItem.getSampleBookItem()
        dao.insertBookItem(book)
        val response = dao.observeAllBooks().getOrAwaitValue()
        assertThat(response).contains(book)

    }

    @Test
    fun insertListOfBookItems() = runBlockingTest {
        val list = arrayListOf<Book>()

        for (i in 1..20) {
            val book = MockBookItem.getSampleBookItem()
            //change primary key of objects to be unique
            book.book_uri = i.toString()
            list.add(book)
        }
        dao.insertOrUpdateAllBooks(list)
        val response = dao.observeAllBooks().getOrAwaitValue()

        assertThat(response.size).isEqualTo(20)

    }
}