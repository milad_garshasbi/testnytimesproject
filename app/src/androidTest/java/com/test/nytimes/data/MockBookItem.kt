package com.test.nytimes.data

import com.google.gson.Gson
import com.test.nytimes.data.local.Book

class MockBookItem() {

    companion object {
        fun getSampleBookItem(): Book {
            val bookItemJsonString = "  {\n" +
                    "        \"rank\": 1,\n" +
                    "        \"rank_last_week\": 0,\n" +
                    "        \"weeks_on_list\": 1,\n" +
                    "        \"asterisk\": 0,\n" +
                    "        \"dagger\": 0,\n" +
                    "        \"primary_isbn10\": \"isbn10 mus\",\n" +
                    "        \"primary_isbn13\": \"9781984818461\",\n" +
                    "        \"publisher\": \"Delacorte\",\n" +
                    "        \"description\": \"Jack Reacher intervenes on an ambush in Tennessee and uncovers a conspiracy.\",\n" +
                    "        \"price\": 0,\n" +
                    "        \"title\": \"THE SENTINEL\",\n" +
                    "        \"author\": \"Lee Child and Andrew Child\",\n" +
                    "        \"contributor\": \"by Lee Child and Andrew Child\",\n" +
                    "        \"contributor_note\": \"\",\n" +
                    "        \"book_image\": \"https://s1.nyt.com/du/books/images/9781984818461.jpg\",\n" +
                    "        \"book_image_width\": 327,\n" +
                    "        \"book_image_height\": 500,\n" +
                    "        \"amazon_product_url\": \"https://www.amazon.com/dp/1984818465?tag=NYTBSREV-20&tag=NYTBS-20\",\n" +
                    "        \"age_group\": \"\",\n" +
                    "        \"book_review_link\": \"\",\n" +
                    "        \"first_chapter_link\": \"\",\n" +
                    "        \"sunday_review_link\": \"\",\n" +
                    "        \"article_chapter_link\": \"\",\n" +
                    "        \"isbns\": [\n" +
                    "          {\n" +
                    "            \"isbn10\": \"isbn10 mus\",\n" +
                    "            \"isbn13\": \"9781101599044\"\n" +
                    "          },\n" +
                    "          {\n" +
                    "            \"isbn10\": \"0593339983\",\n" +
                    "            \"isbn13\": \"9780593339985\"\n" +
                    "          },\n" +
                    "          {\n" +
                    "            \"isbn10\": \"0593339711\",\n" +
                    "            \"isbn13\": \"9780593339718\"\n" +
                    "          }\n" +
                    "        ],\n" +
                    "        \"buy_links\": [\n" +
                    "          {\n" +
                    "            \"name\": \"Amazon\",\n" +
                    "            \"url\": \"https://www.amazon.com/dp/1984818465?tag=NYTBSREV-20&tag=NYTBS-20\"\n" +
                    "          },\n" +
                    "          {\n" +
                    "            \"name\": \"Apple Books\",\n" +
                    "            \"url\": \"https://du-gae-books-dot-nyt-du-prd.appspot.com/buy?title=THE+SENTINEL&author=Lee+Child+and+Andrew+Child\"\n" +
                    "          },\n" +
                    "          {\n" +
                    "            \"name\": \"Barnes and Noble\",\n" +
                    "            \"url\": \"https://www.anrdoezrs.net/click-7990613-11819508?url=https%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781984818461\"\n" +
                    "          },\n" +
                    "          {\n" +
                    "            \"name\": \"Books-A-Million\",\n" +
                    "            \"url\": \"https://www.anrdoezrs.net/click-7990613-35140?url=https%3A%2F%2Fwww.booksamillion.com%2Fp%2FTHE%2BSENTINEL%2FLee%2BChild%2Band%2BAndrew%2BChild%2F9781984818461\"\n" +
                    "          },\n" +
                    "          {\n" +
                    "            \"name\": \"Bookshop\",\n" +
                    "            \"url\": \"https://bookshop.org/a/3546/9781984818461\"\n" +
                    "          },\n" +
                    "          {\n" +
                    "            \"name\": \"Indiebound\",\n" +
                    "            \"url\": \"https://www.indiebound.org/book/9781984818461?aff=NYT\"\n" +
                    "          }\n" +
                    "        ],\n" +
                    "        \"book_uri\": \"nyt://book/8af67edb-c121-58bc-88ca-4fc6599ef2b6\"\n" +
                    "      }"

            return Gson().fromJson(bookItemJsonString, Book::class.java)
        }
    }
}