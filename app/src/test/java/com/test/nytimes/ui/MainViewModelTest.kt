package com.test.nytimes.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.google.gson.Gson
import com.test.nytimes.MainCoroutineRule
import com.test.nytimes.data.local.Book
import com.test.nytimes.data.remote.ApiService
import com.test.nytimes.data.remote.responses.BookListResult
import com.test.nytimes.repository.FakeBooksRepository
import com.test.nytimes.utils.Resource

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import retrofit2.Response
import javax.inject.Inject

@ExperimentalCoroutinesApi
class MainViewModelTest {
    @Mock
    lateinit var apiService: ApiService

    @Mock
    lateinit var observer: Observer<List<Book>>

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()


    private lateinit var fakeBooksRepository: FakeBooksRepository

    private lateinit var viewModel: MainViewModel

    @Before
    fun setup() {

        MockitoAnnotations.initMocks(this)
        fakeBooksRepository=FakeBooksRepository()
        viewModel = MainViewModel(fakeBooksRepository)

    }

    @Test
    fun testSuccess_getBookListResult() {
        runBlockingTest {
            val result = mockBookResult("response.json")
            `when`(apiService.getBooks()).thenReturn(Response.success(result))
            viewModel.getBooks().observeForever(observer)
            verify(observer).onChanged(result.results.books)
        }
    }


    private fun mockBookResult(fileName: String): BookListResult {
        val file = ClassLoader.getSystemResource(fileName).readText()
        return Gson().fromJson(file, BookListResult::class.java)

    }

}