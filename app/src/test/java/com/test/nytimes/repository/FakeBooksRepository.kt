package com.test.nytimes.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.test.nytimes.data.local.Book
import com.test.nytimes.data.remote.responses.BookListResult
import com.test.nytimes.repositories.BooksRepository
import com.test.nytimes.utils.Resource
import javax.inject.Inject

class FakeBooksRepository @Inject constructor() : BooksRepository {

    private val bookItems = mutableListOf<Book>()
    private val observableShoppingItems = MutableLiveData<List<Book>>(bookItems)

    override fun observeAllBooksFromDb(): LiveData<List<Book>> {

        val result = Gson().fromJson(
            ClassLoader.getSystemResource("response.json").readText(),
            BookListResult::class.java
        )
        observableShoppingItems.postValue(result.results.books)

        return observableShoppingItems
    }

    override suspend fun insertOrUpdateAllBooks(books: List<Book>) {
        bookItems.addAll(books)
        refreshLiveData()
    }

    override suspend fun fetchBooksResultRemotely(): Resource<BookListResult> {
        val file = ClassLoader.getSystemResource("response.json").readText()
        val result = Gson().fromJson(file, BookListResult::class.java)
        return Resource.success(result)

    }

    private fun refreshLiveData() {
        observableShoppingItems.postValue(bookItems)

    }
}
