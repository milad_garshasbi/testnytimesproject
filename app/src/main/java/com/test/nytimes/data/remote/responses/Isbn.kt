package com.test.nytimes.data.remote.responses

data class Isbn(
    val isbn10: String,
    val isbn13: String
)