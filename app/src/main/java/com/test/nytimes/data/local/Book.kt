package com.test.nytimes.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.test.nytimes.data.remote.responses.BuyLink
import com.test.nytimes.data.remote.responses.Isbn
import com.test.nytimes.utils.BookTypeConverters

@Entity(tableName = "books")
data class Book(
    val age_group: String,
    val amazon_product_url: String,
    val article_chapter_link: String,
    val asterisk: Int,
    val author: String,
    val book_image: String,
    val book_image_height: Int,
    val book_image_width: Int,
    val book_review_link: String,
    @PrimaryKey
    var book_uri: String,
    val buy_links: List<BuyLink>,
    val contributor: String,
    val contributor_note: String,
    val dagger: Int,
    val description: String,
    val first_chapter_link: String,
    val isbns: List<Isbn>,
    val price: Int,
    val primary_isbn10: String,
    val primary_isbn13: String,
    val publisher: String,
    val rank: Int,
    val rank_last_week: Int,
    val sunday_review_link: String,
    val title: String,
    val weeks_on_list: Int
)