package com.test.nytimes.data.remote.responses

data class BookListResult(
    val copyright: String,
    val last_modified: String,
    val num_results: Int,
    val results: Results,
    val status: String
)