package com.test.nytimes.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.test.nytimes.utils.BookTypeConverters

@Database(entities = [Book::class], version = 1)
@TypeConverters(BookTypeConverters::class)
abstract class BookItemsDataBase : RoomDatabase() {
    abstract fun booksDao(): BookItemDao
}