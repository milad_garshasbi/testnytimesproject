package com.test.nytimes.data.remote

import com.test.nytimes.data.remote.responses.BookListResult
import com.test.nytimes.utils.Constants
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("{date}/{list}.json")
    suspend fun getBooks(
        @Path("date") date: String = "current",
        @Path("list") list: String = "hardcover-fiction",
        @Query("offset") offset: Int = 0,
        @Query("api-key") apiKey: String = Constants.API_KEY
    )
            : Response<BookListResult>

}