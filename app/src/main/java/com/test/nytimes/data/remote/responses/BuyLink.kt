package com.test.nytimes.data.remote.responses

data class BuyLink(
    val name: String,
    val url: String
)