package com.test.nytimes.data.local

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface BookItemDao {

    @Query("Select * From books")
    fun observeAllBooks(): LiveData<List<Book>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOrUpdateAllBooks(book: List<Book>)


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBookItem(book: Book)

}