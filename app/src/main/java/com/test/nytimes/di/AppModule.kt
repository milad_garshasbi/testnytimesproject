package com.test.nytimes.di

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.test.nytimes.R
import com.test.nytimes.data.local.BookItemDao
import com.test.nytimes.data.local.BookItemsDataBase
import com.test.nytimes.data.remote.ApiService
import com.test.nytimes.repositories.BooksRepository
import com.test.nytimes.repositories.DefaultBooksRepository
import com.test.nytimes.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {


    @Singleton
    @Provides
    fun booksDataBase(@ApplicationContext context: Context) =
        Room.databaseBuilder(context, BookItemsDataBase::class.java, Constants.DATABASE_NAME).build()


    @Singleton
    @Provides
    fun provideBookItemDao(dataBase: BookItemsDataBase) = dataBase.booksDao()

    @Singleton
    @Provides
    fun provideGlideInstance(@ApplicationContext context: Context) =
        Glide.with(context).setDefaultRequestOptions(
            RequestOptions()
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background)
        )

    @Singleton
    @Provides
    fun provideApiService(): ApiService {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Constants.BASE_URL)
            .build().create(ApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideBooksRepository(apiService: ApiService, bookItemDao: BookItemDao) =
        DefaultBooksRepository(apiService, bookItemDao) as BooksRepository
}