package com.test.nytimes.utils

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.test.nytimes.data.remote.responses.BuyLink
import com.test.nytimes.data.remote.responses.Isbn
import java.lang.reflect.Type

class BookTypeConverters {

    private val gson = Gson()

    @TypeConverter
    fun fromBuyLinkListToString(list: List<BuyLink>) = gson.toJson(list)

    @TypeConverter
    fun fromStringToBuyLinkList(data: String): List<BuyLink> {
        val listType: Type = object : TypeToken<List<BuyLink?>?>() {}.type
        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun fromIsbnListToString(list: List<Isbn>) = gson.toJson(list)

    @TypeConverter
    fun fromStringToIsbnList(data: String): List<Isbn> {
        val listType: Type = object : TypeToken<List<Isbn?>?>() {}.type
        return gson.fromJson(data, listType)

    }

}