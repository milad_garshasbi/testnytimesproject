package com.test.nytimes.utils

object Constants {
    const val DATABASE_NAME="books_db"
    const val DATE_FORMAT="yyyy-MM-dd'T'HH:mm:ssX"
    const val API_KEY = "dG7cVYcE8G0CHkgpnS9Fb6jJn9vd32Mm"
    const val BASE_URL = "https://api.nytimes.com/svc/books/v3/lists/"
    const val UPDATE_PERIOD_DAYS = 7

}
