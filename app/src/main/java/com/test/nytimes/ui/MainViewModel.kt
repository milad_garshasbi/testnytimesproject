package com.test.nytimes.ui

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.test.nytimes.data.local.Book
import com.test.nytimes.data.remote.responses.BookListResult
import com.test.nytimes.repositories.BooksRepository
import com.test.nytimes.repositories.DefaultBooksRepository
import com.test.nytimes.utils.Constants
import com.test.nytimes.utils.Resource
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class MainViewModel @ViewModelInject constructor(
    private val repository: BooksRepository


) : ViewModel() {

    private val status = MutableLiveData(Resource.loading(null))
    private val books: LiveData<List<Book>> = repository.observeAllBooksFromDb()
    private lateinit var job: Job

    fun getStatus() = status

    fun getBooks() = books

    fun fetchData() {
        job = viewModelScope.launch {
            val response = repository.fetchBooksResultRemotely()
            if (response.data != null) {
                repository.run {
                    if (dataHasChanged(response.data.last_modified)
                        || books.value!!.isEmpty()
                    ) {
                        status.value = Resource.success(null)
                        insertOrUpdateAllBooks(books = response.data.results.books)
                    }
                }

            } else status.value = Resource.error(response.message.toString(), null)
        }
    }

    private fun dataHasChanged(lastModified: String): Boolean {
        return try {
            val simpleDateFormat = SimpleDateFormat(Constants.DATE_FORMAT, Locale.getDefault())
            val lastModifiedDate = simpleDateFormat.parse(lastModified)
            //update weekly
            val diff = (Date().time - lastModifiedDate.time) / 60 / 60 / 24 / 1000;
            diff >= Constants.UPDATE_PERIOD_DAYS;
        } catch (e: Exception) {
            false
        }

    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }

}