package com.test.nytimes.ui


import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.test.nytimes.R
import com.test.nytimes.ui.adapter.BooksAdapter
import com.test.nytimes.utils.Status
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.rxjava3.disposables.Disposable
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


private const val TAG = "MainActivity"

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var booksAdapter: BooksAdapter

    lateinit var disposable: Disposable

    private val mainViewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        subscribeToObserver()
        setUpRecyclerView()


    }

    private fun subscribeToObserver() {
        mainViewModel.fetchData()
        mainViewModel.getStatus().observe(this, Observer {
            if (it.status == Status.LOADING)
                progressBar.visibility = View.VISIBLE
            else {
                //hide loading when success or failed loading data
                progressBar.visibility = View.GONE

            }
        })

        mainViewModel.getBooks().observe(this, Observer {
            if (it != null && it.isNotEmpty()) {
                booksAdapter.books = it
                progressBar.visibility = View.GONE
            }
        })


    }

    private fun setUpRecyclerView() {
        recyclerView_main.apply {
            adapter = booksAdapter
            disposable = booksAdapter.clickEvent
                .subscribe {
                    Toast.makeText(
                        context,
                        it.title,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            layoutManager = LinearLayoutManager(context)

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
    }
}