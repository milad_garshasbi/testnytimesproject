package com.test.nytimes.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.test.nytimes.R
import com.test.nytimes.data.local.Book
import io.reactivex.rxjava3.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_book_layout.view.*
import javax.inject.Inject

class BooksAdapter @Inject constructor(
    private val glide: RequestManager
) : RecyclerView.Adapter<BooksAdapter.BookItemViewHolder>() {

    private val clickSubject = PublishSubject.create<Book>()

    val clickEvent = clickSubject!!

    private val diffCallback = object : DiffUtil.ItemCallback<Book>() {
        override fun areItemsTheSame(oldItem: Book, newItem: Book): Boolean {
            return oldItem.book_uri == newItem.book_uri
        }

        override fun areContentsTheSame(oldItem: Book, newItem: Book): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    var books: List<Book>
        get() = differ.currentList
        set(value) {
            differ.submitList(value)
        }

    class BookItemViewHolder(view: View) : RecyclerView.ViewHolder(view)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookItemViewHolder {
        return BookItemViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_book_layout, parent, false)

        )
    }

    override fun onBindViewHolder(holder: BookItemViewHolder, position: Int) {
        val item = books[position]

        holder.itemView.setOnClickListener {
            clickSubject.onNext(item)
        }
        glide.load(item.book_image).into(holder.itemView.imageView)
        holder.itemView.imageView
        holder.itemView.txt_title.text = item.title
        holder.itemView.txt_description.text = item.description


    }

    override fun getItemCount() = books.size
}


