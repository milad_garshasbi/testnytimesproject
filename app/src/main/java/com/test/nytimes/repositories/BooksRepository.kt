package com.test.nytimes.repositories

import androidx.lifecycle.LiveData
import com.test.nytimes.data.local.Book
import com.test.nytimes.data.remote.responses.BookListResult
import com.test.nytimes.utils.Resource

interface BooksRepository {

    fun observeAllBooksFromDb(): LiveData<List<Book>>

    suspend fun insertOrUpdateAllBooks(books:List<Book>)

    suspend fun fetchBooksResultRemotely(): Resource<BookListResult>
}