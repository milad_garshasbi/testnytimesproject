package com.test.nytimes.repositories

import androidx.lifecycle.LiveData
import com.test.nytimes.data.remote.ApiService
import com.test.nytimes.data.local.Book
import com.test.nytimes.data.local.BookItemDao
import com.test.nytimes.data.remote.responses.BookListResult
import com.test.nytimes.utils.Resource
import io.reactivex.rxjava3.schedulers.Schedulers.io
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import okhttp3.Dispatcher
import java.lang.Exception
import javax.inject.Inject

class DefaultBooksRepository @Inject constructor(
    private val apiService: ApiService,
    private val bookItemDao: BookItemDao
) : BooksRepository {
    override fun observeAllBooksFromDb(): LiveData<List<Book>> {
        return bookItemDao.observeAllBooks()
    }

    override suspend fun insertOrUpdateAllBooks(books: List<Book>) {
        bookItemDao.insertOrUpdateAllBooks(books)
    }

    override suspend fun fetchBooksResultRemotely(): Resource<BookListResult> {
        return try {
            val response = apiService.getBooks()
            if (response.isSuccessful) {
                response.body()?.let {
                    return@let Resource.success(it)
                }
                    ?: Resource.error("Empty Result", null)
            } else return Resource.error("Error Fetching Data", null)

        } catch (e: Exception) {
            return Resource.error("Error Occurred", null)
        }
    }
}